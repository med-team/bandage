bandage (0.9.0-2) unstable; urgency=medium

  * Fix watch file
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 01 Nov 2022 19:40:18 +0100

bandage (0.9.0-1) unstable; urgency=medium

  * New upstream version
  * Add missing build dependency on dh addon.

 -- Andreas Tille <tille@debian.org>  Sun, 16 Jan 2022 08:29:19 +0100

bandage (0.8.1-5) unstable; urgency=medium

  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.0 (routine-update)
  * Apply multi-arch hints.
    + bandage-examples: Add Multi-Arch: foreign.

 -- Andreas Tille <tille@debian.org>  Wed, 06 Oct 2021 16:48:17 +0200

bandage (0.8.1-4) unstable; urgency=medium

  * Make sure architecture any builds will not fail
    Closes: #968885

 -- Andreas Tille <tille@debian.org>  Wed, 09 Sep 2020 11:20:12 +0200

bandage (0.8.1-3) unstable; urgency=medium

  * Source-only upload

 -- Andreas Tille <tille@debian.org>  Wed, 19 Aug 2020 15:34:14 +0200

bandage (0.8.1-2) unstable; urgency=medium

  * Team upload

  [ Valentin Marcon ]
  * Correct DOI

  [ Steffen Möller ]
  * Added ref to conda

  [ Pranav Ballaney ]
  * Add autopkgtests
  * Install docs

  [ Andreas Tille ]
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/control (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Fix permissions of examples
  * Fix copyright

 -- Pranav Ballaney <ballaneypranav@gmail.com>  Fri, 29 May 2020 20:57:37 +0530

bandage (0.8.1-1) unstable; urgency=medium

  * Initial release (Closes: #872622)

 -- Andreas Tille <tille@debian.org>  Mon, 27 Aug 2018 11:41:01 +0200
